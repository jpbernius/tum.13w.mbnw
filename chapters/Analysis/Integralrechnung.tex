%!TEX root = ../../MBNWskript.tex
\subsection{Integralrechnung} % (fold)
\label{sub:integralrechnung}

Gegeben: Funktion
\begin{math}
	f:\left(a,b\right) \rightarrow \mathbb{R}
\end{math}
.\\
Gesucht: Flächeninhalt der Fläche zwischen Graph($f$) und der $x$-Achse
\subsubsection{Definition} % (fold)
\label{ssub:integralrechnung-definition}

Seien $a,c \in \mathbb{R}, a<b, x_0 = a < x_1 < x_2 < ... < x_n = b, f:[a,b] \to \mathbb{R}$ Funktionen.
\begin{enumerate}[label=(\alph*)]
	\item Wir nennen $Z=\{x_0,x_1,...,x_n\}$ eine \underline{Zerlegung} von $[a,b]$ (in die Teilintervalle $[x_0,x_1],[x_1,x_2],...[x_{n-1},x_n]$) und setzen $\Delta Z := max\{|x_k-x_{k-1}| |k=1,2,...,n\}$
	\item Für $z=(z^{(1)},z^{(2)},...,z^{(n)}) \in \mathbb{R}^n$ ist $z^{(k)} \in [x_{k-1},x_k]$ für $k=1,2,...,n$ heißt $S_f(Z,z) = \sum ^n_{k=1}f(z_k)(x_k-x_{k-1})$ eine \underline{Zwischensumme von $f$ zur Zerlegung $Z$}.
\end{enumerate}

\underline{Skizze}: ($n=5$) ~Abbildung \ref{fig:integralrechnung-definition-skizze}
\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.75\textwidth]{LineareAlgebra/integralrechnung/defintion-skizze.png}
	\label{fig:integralrechnung-definition-skizze}
	\caption{$S_f(Z,z)$ = Flächeninhalt der von den Balken überdeckten Fläche}
\end{figure}
% \FloatBarrier
% subsubsection definition (end)

\subsubsection{Definition} % (fold)
\label{ssub:integralrechnung-definition2}

\fbox{
\parbox{\columnwidth}{
Seien $a,c \in \mathbb{R}$, $a<b$ eine Funktion $f:[a,b] \to \mathbb{R}$ heißt (auf $[a,b]$) \underline{integrierbar}, wenn es ein $A\in\mathbb{R}$ gibt, so dass für \underline{\textbf{jede}} Folge $(S_f(Z_j,z_j))_{j\in\mathbb{N}}$ von Zwischensummen zur Zerlegung $Z_j$ mit $\Delta Z_j \xrightarrow[j\to\infty]{}0$ gilt:
\[
	\lim_{j\to\infty} S_f(Z_j,z_j)=A
\]
In diesem fall ist $A$ eindeutig bestimmt und heißt das \underline{(bestimmte) Integral} von $f$ über $[a,b]$. Schreibweise: \[
	\int_a^b f(x)dx := A
\]
}}
% subsubsection definition2 (end)


\subsubsection{Anmerkung} % (fold)
\label{ssub:integralrechnung-anmerkung}

\begin{enumerate}[label=(\alph*)]
	\item Setze $\int_b^a f(x)dx := -\int_a^b f(x)dx$, $\int_a^a f(x)dx = 0$.
	\item Das in (\ref{ssub:integralrechnung-definition2}) definierte Integral nennt man auch \underline{Riemann-Integral}. Es gibt eine äquivalente Definition über sogenannte Ober-/Untersummen.
	\item Ist $f:[a,b]\to\mathbb{R}_{\geq 0}$ integrierbar, so lässt sich $\int_a^b f(x)dx$ als der Inhalt der Fläche zwischen Graph($f$) und $x$-Achse interpretieren. (siehe Abb. \ref{fig:integralrechnung-anmerkung-skizze})

	\begin{figure}[ht!]
		\centering
		\includegraphics[scale=0.75]{LineareAlgebra/integralrechnung/anmerkung-skizze.png}
		\label{fig:integralrechnung-anmerkung-skizze}
		\caption{Skizze zu \ref{ssub:integralrechnung-anmerkung}(c)}
	\end{figure}

	Allgemein: $\int_a^b f(x)dx$ = "`vorzeichengewichteter Flächeninhalt"'\\
	$[$Flächen unterhalb der $x$-Achse gehen negativ ein.$]$
	\[
		\int_a^b f(x)dx = A_1-A_2+A_3
	\]
	(siehe Abb. \ref{fig:integralrechnung-anmerkung-skizze-2})
	\begin{figure}[ht!]
		\centering
		\includegraphics[scale=0.75]{LineareAlgebra/integralrechnung/anmerkung-skizze-2.png}
		\label{fig:integralrechnung-anmerkung-skizze-2}
		\caption{Skizze zu \ref{ssub:integralrechnung-anmerkung} - Allgemein}
	\end{figure}
\end{enumerate}
% subsubsection anmerkung (end)

\subsubsection{Wichtige Klassen integrierbarer Funktionen} % (fold)
\label{ssub:integralrechnung-wichtige_klassen_integrierbarer_funktionen}

\fbox{
\parbox{\columnwidth}{
Seien $a,b\in\mathbb{R},a<b$, $f:[a,b]\to\mathbb{R}$ Funktionen.
\begin{enumerate}[label=(\alph*)]
	\item Ist $f$ beschränkt und gibt es hächstens endlich viele $x\in[a,b]$, in denen $f$ nicht stetig ist, so ist $f$ integrierbar. Insbesondere: Jede stetige Funktion $f:[a,b]\to\mathbb{R}$ ist integrierbar.
	\item Ist $f$ monoton wachsend oder monoton fallend, so ist $f$ integrierbar.
\end{enumerate}
}}
% subsubsection wichtige_klassen_integrierbarer_funktionen (end)

\vspace{7.5mm}

\subsubsection{Eigenschaften integrierbarer Funktionen} % (fold)
\label{ssub:integralrechnung-eigenschaften_integrierbarer_funktionen}

\fbox{
\parbox{\columnwidth}{
Seien $a,b\in\mathbb{R}$, $a<b$, $f,g:[a,b]\to\mathbb{R}$ integrierbare Funktionen. 
Dann gilt:
\begin{enumerate}[label=(\alph*)]
	\item Für alle $\lambda,\mu\in\mathbb{R}: \int_a^b \left(\lambda f(x)+ \mu g(x)\right)dx = \lambda\int_a^b f(x)dx + \mu\int_a^b g(x)dx$.
	\item Für alle $c\in[a,b]: \int_a^b f(x)dx = \int_a^c f(x)dx + \int_c^b f(x)dx$. (siehe Abbildung \ref{fig:integralrechnung-eigenschaften_integrierbarer_funktionen-figb})
	\item Sind $m,M\in\mathbb{R}$ mit $m\leq f(x) \leq M$ für alle $x\in[a,b]$, so ist $m\cdot\left( b-a\right) \leq \int_a^b f(x)dx\leq M\cdot (b-a)$. (siehe Abbildung \ref{fig:integralrechnung-eigenschaften_integrierbarer_funktionen-figc})
	\item Ist $f(x) \leq g(x)$ für alle $x\in[a,b]$, so ist $\int_a^b f(x)dx \leq \int_a^b g(x)dx$. (siehe Abbildung \ref{fig:integralrechnung-eigenschaften_integrierbarer_funktionen-figd})\\("`Monotonie"')
	\item $\left|\int_a^b f(x)dx\right| \leq \int_a^b \left| f(x)\right| dx$ ("`Dreiecksgleichung"')
\end{enumerate}
}}
\begin{figure}[ht!]
	\centering
	\minipage{0.3\textwidth}
		\includegraphics[width=\linewidth]{LineareAlgebra/integralrechnung/eigenschaften_integrierbarer_funktionen-figb.png}
		\caption{Skizze zu (\ref{ssub:integralrechnung-eigenschaften_integrierbarer_funktionen})(b)}
		\label{fig:integralrechnung-eigenschaften_integrierbarer_funktionen-figb}
	\endminipage\hfill
	\minipage{0.3\textwidth}
		\includegraphics[width=\linewidth]{LineareAlgebra/integralrechnung/eigenschaften_integrierbarer_funktionen-figc.png}
		\caption{Skizze zu (\ref{ssub:integralrechnung-eigenschaften_integrierbarer_funktionen})(c)}
		\label{fig:integralrechnung-eigenschaften_integrierbarer_funktionen-figc}
	\endminipage\hfill
	\minipage{0.3\textwidth}
		\includegraphics[width=\linewidth]{LineareAlgebra/integralrechnung/eigenschaften_integrierbarer_funktionen-figd.png}
		\caption{Skizze zu (\ref{ssub:integralrechnung-eigenschaften_integrierbarer_funktionen})(d)}
		\label{fig:integralrechnung-eigenschaften_integrierbarer_funktionen-figd}
	\endminipage\hfill
\end{figure}
% subsubsection eigenschaften_integrierbarer_funktionen (end)

\subsubsection*{Berechnung von Integralen:}

\subsubsection{Definition} % (fold)
\label{ssub:integralrechnung-definition-3}

\fbox{
\parbox{\columnwidth}{
Sei $I\subseteq\mathbb{R}$ Intervall, $f:I\to\mathbb{R}$ Funktion. Eine differenzierbare Funktion $F:I\to\mathbb{R}$ heißt eine \underline{Stammfunktion} von $f$, falls $F'(x)=f(x)$ für alle $x\in I$. In diesem Fall schreibt man auch $\int f(x)dx=F(x)$ ("`unbestimmtes Integral"').
}}


\paragraph{Anmerkung:} Ist $F$ Stammfunktion von $f$, so ist jede Stammfunktion von $f$ von der Form $F+c$ ($c\in\mathbb{R}$ Konstante).\\
% subsubsection definition3 (end)

\subsubsection{Hauptsatz der Differential- und Integralrechnung} % (fold)
\label{ssub:integralrechnung-hauptsatz_der_differential_und_integralrechnung}

\fbox{
\parbox{\columnwidth}{
Seien $a,b\in\mathbb{R}$, $a<b$, $f:[a,b]\to\mathbb{R}$ eine stetige Funktion. Dann gilt:
\begin{enumerate}[label=(\alph*)]
	\item $f$ hat eine Stammfunktion, z.B, $F:[a,b]\to\mathbb{R}, F(x) := \int_a^x f(t)dt$
	\item (Integralberechnung) Sei $F$ eine (beliebige) Stammfunktion von $f$. Dann gilt: \[\int_a^b f(x)dx = F(b)-F(a)\]\[=:F(x)\mid_a^b\]
\end{enumerate}
}}
% subsubsection hauptsatz_der_differential_und_integralrechnung (end)


\subsubsection*{Wie findet man Stammfunktionen?}

\subsubsection{Wichtige Stammfunktionen} % (fold)
\label{ssub:integralrechnung-wichtige_stammfunktionen}

\fbox{
\parbox{\columnwidth}{
($f$:Funktion, $F$:Stammfunktion (\ref{ssub:integralrechnung-definition-3}) von $f$)


\minipage{0.49\textwidth}
	\begin{math}
		\begin{array}{rl|c}
			f(x)&&F(x)\\
		\hline
			x^\alpha&(x\neq -1)&\frac{1}{\alpha+1}\cdot x^{\alpha+1}\\
			\frac{1}{x}&(x\neq 0)&\ln \left| x \right| \\
			e^x&&e^x\\
			a^x&(a\in \mathbb{R}_{>0}\backslash\{1\})&\frac{a^x}{\ln(a)}
		\end{array}
	\end{math}
\endminipage\hfill
\minipage{0.49\textwidth}
	\begin{math}
		\begin{array}{rl|c}
			f(x)&&F(x)\\
		\hline
			\sin(x)&&-\cos(x)\\
			\cos(x)&&\sin(x)\\
			\frac{1}{1+x^2}&&\arctan(x)\\
			\frac{1}{\sqrt{1-x^2}}&(-1<x<1)&\arcsin(x)
		\end{array}
	\end{math}
\endminipage\hfill

}}
% subsubsection wichtige_stammfunktionen (end)

\subsubsection{Hilfsmittel zur Berechnung von Integralen/Stammfunktionen} % (fold)
\label{ssub:integralrechnung-hilfsmittel_zur_berechnung_von_integralen_stammfunktionen}

\fbox{
\parbox{\columnwidth}{
Seien $I\subseteq\mathbb{R}$ Intervall, $a,b\in I$, $f,g:I\to\mathbb{R}$ Funktionen, so dass folgende Integrale, Ableitungen, etc. existieren. Dann gilt:

\begin{enumerate}[label=(\alph*)]
	\item $\int{(\lambda f(x) + \mu g(x)}dx = \lambda \int{f(x)}dx + \mu \int{g(x)}dx$ \hspace{5mm}($\lambda , \mu \in \mathbb{R}$ Konstanten)
	\item "`partielle Integration"'
		\begin{enumerate}[label=(\roman*)]
			\item $\int{f'(x)\cdot g(x)}dx = f(x)\cdot g(x)-\int{f(x)\cdot g'(x)}dx$
			\item $\int_a^b{f'(x)\cdot g'(x)}dx = f(x)\cdot g(x)\mid_a^b - \int_a^b{f(x)\cdot g'(x)}dx$
		\end{enumerate}
	\item "`Substitution"'
		\begin{enumerate}[label=(\roman*)]
			\item $\int{f(g(x))\cdot g'(x)}dx = F(g(x))$, wobei F eine (beliebige) Stammfunktion von $f$ ist.
			\item $\int_a^b{f(g(x))\cdot g'(x)}dx = \int_{g(a)}^{g(b)}{f(t)}dt$.
		\end{enumerate}
\end{enumerate}
}}
\paragraph{Anmerkung:} (\ref{ssub:integralrechnung-hilfsmittel_zur_berechnung_von_integralen_stammfunktionen})(c)(i) wird oft benutzt:
	\begin{enumerate}
		\item Ersetzte $g(x)\to t$, $g'(x)dx \to dt$
		\item Berechne $\int{f(t)}dt$
		\item "`Rücksubstituiere"' $t \to g(x)$.
	\end{enumerate}

% subsubsection hilfsmittel_zur_berechnung_von_integralen_stammfunktionen (end)

\subsubsection{Beispiele} % (fold)
\label{ssub:integralrechnung-beispiele}
\begin{enumerate}[label=(\arabic*)]
	\item $\int{\frac{1}{x^4}}dx=-\frac{1}{3}x^{-3}=\underline{-\frac{1}{3x^3}}$, $\int{\sqrt{x}}dx=\underline{\frac{2}{3}x^{\frac{3}{2}}}$
	\item $\int{x\cdot\sin(x)dx} \overset{part. Int.}{=} \left( -\cos(x)\right) \cdot x - \int{1\cdot\left(-\cos(x)\right)} dx = -x\cdot\cos(x) + \int{\cos(x)}dx = \underline{\sin(x) - x\cdot \cos(x)}$.
	\item $\int{x^2 \cdot e^x}dx \overset{part. Int.}{=} x^2 \cdot e^x - \int{2x \cdot e^x}dx = x^2 \cdot e^x - 2 \cdot \int{x \cdot e^x}dx \overset{part. Int.}{=} x^2 \cdot e^x - 2 \cdot \left( x\cdot e^x - \int{1 \cdot e^x}dx \right) = x^2 e^x - 2\cdot x\cdot e^x + 2 \cdot e^x = \underline{\left(x^2 - 2 \cdot x + 2\right) \cdot e^x}$.
	\item fehlt
	\item $\int{\cos(x) \cdot e^{\sin(x)}}dx = \int{e^{\sin(x)} \cdot \cos(x)}dx = (subst.) \left( \begin{smallmatrix} t = \sin(x)\\dt=\cos(x) \end{smallmatrix} \right) \int{e^t}dt=e^t = \underline{e^{\sin(x)}}$.
	\item fehlt
	\item $\int_6^14{\frac{1}{\sqrt{3x+7}}}dx = \frac{1}{3} \int_6^14{\frac{1}{\sqrt{3x+7}}\cdot 3}dx = (subst.) \left(\begin{smallmatrix} t=3x+t\\dt=3dx \end{smallmatrix}\right) \frac{1}{3} \int_6^14{...}$fehlt
	\item $\int_1^e{\frac{\ln(x)}{x}}dx = \int_1^e{\ln(x) \cdot \frac{1}{x}}dx = (subst.) \left(\begin{smallmatrix} t=\ln(x)\\dt=\frac{1}{x}dx \end{smallmatrix}\right) \int_{\ln(1)}^{\ln(e)}{t}dt=\frac{1}{2}t^2\mid_0^1 = \frac{1}{2}\cdot 1^2 - \frac{1}{2}\cdot 0 = \underline{\frac{1}{2}}$.
	\item fehlt
\end{enumerate}
% subsubsection beispiele (end)

\paragraph{Beispiel} % (fold)
\label{par:integralrechnung-beispiel}

\begin{enumerate}[label=*]
	\item Betrachte das Einkommen der Personen einer Bevölkerung in einem fest vorgegeben Jahr $J$, $n :=$ Anzahl der Personen in der Bevölkerung ($n$ groß)
	\item $F(n) :=$ Anteil der Bevölkerung mit Einkommen $\leq n$ im Jahr $J$. \\ $\to$ Erhalte Funktion $F:[r_{min}, r_{max}]\to \mathbb{R}$
	\item Annahme: $F$ differenzierbar, $F'=f$ integrierbar
	\item Seien $r_min \leq a < b \leq r_{max}$.\\
	$\Rightarrow$ Anteil der Bevölkerung mit Einkommen zwischen $a$ und $b$: $F(b)-F(a) := \int_a^b{f(x)}dx$\\
	$\Rightarrow$ Anzahl der Personen mit Einkommen zwischen $a$ und $b$: $n\cdot \int_a^b{f(x)}dx$.
	\item \underline{Gesucht}: Formel für das Gesamteinkommen der Personen mit Einkommen zwischen $a$ und $b$.
	\item \underline{Hierzu}: $M(r) =$ Gesamteinkommen der Personen mit Einkommen $\leq r$.\\
	Also gesucht: $M(b)-M(a)$ \vspace{7.5mm} (Idee: $M(b)-M(a) = \int_a^b{M'(r)}dr$).\\
	Sei $r \in [r_{min}, r_{max}]$, $r~$ nahe bei $r$.\\
	$M(r~)-M(r) ~~ r \cdot (\text{Anzahl Personen mit Einkommen zwischen $r~$ und $r$})$
	FEHLT
\end{enumerate}
% paragraph beispiel (end)

\paragraph{Wozu sind Integrale noch gut?} % (fold)
\label{par:integralrechnung-wozu_sind_integrale_noch_gut}
\begin{enumerate}[label=(\arabic*)]
	\item Spielen wichtige Rolle in der Wahrscheinlichkeitstheorie und in der Statistik
	\item Wichtiges Werkzeug zur Lösung von Differentialgleichungen.
\end{enumerate}
% paragraph wozu_sind_integrale_noch_gut (end)

% subsection integralrechnung (end)

\subsection*{Uneigentliche Integrale} % (fold)
\label{sub:uneigentliche_integrale}

Bisher: $\int_a^b{f(x)}dx$ mit $a,b\in \mathbb{R}$. In Wahrscheinlichkeitstheorie und Statistik tauchen oft Integrale $\int_{-\infty}^b{f(x)}dx$, $\int_{-\infty}^\infty{f(x)}dx$ auf.

\subsubsection{Definition} % (fold)
\label{ssub:uneigentliche_integrale-definition}

\fbox{
\parbox{\columnwidth}{
(Uneigentliche Integrale)
\begin{enumerate}[label=(\arabic*)]
	\item Für
	\begin{enumerate}[label=(\roman*)]
		\item $f:[a,\infty) \to \mathbb{R}$ setze $\int_a^\infty{f(x)}dx := \lim_{b\to\infty}{\int_a^b{f(x)}dx}$
		\item fehlt
		\item $f:[a,b) \to \mathbb{R}$ setze $\int_a^b{f(x)}dx := \lim_{t \to b-}{\int_a^t{f(x)}dx}$
		\item fehlt
	\end{enumerate}
	falls die jeweilige rechts Seite ($Grenzwert \in \mathbb{R}$, $Integrale$) existiert.
	\item Seien $a \in \mathbb{R} \cup \{ -\infty \}$, $b\in \mathbb{R} \cup \{\infty\}$, $f:(a,b) \to \mathbb{R}$. Setze $\int_a^b{f(x)}dx :=$
\end{enumerate}
}}
% subsubsection definition (end)

\paragraph{Anmerkung} % (fold)
\label{par:uneigentliche_integrale-anmerkung}

Falls ein uneigentliches Integral im Sinne von (\ref{ssub:uneigentliche_integrale-definition}) existiert, so sagt man auch, dass es \underline{konvergiert}, sonst dass es \underline{divergiert}.
% paragraph anmerkung (end)

\subsubsection{Beispiele} % (fold)
\label{ssub:uneigentliche_integrale-beispiele}

\begin{enumerate}[label=(\arabic*)]
	\item $\int_0^\infty{e^{-x}}dx=?$ $\int_0^b{e^{-x}}dx=-e^{-x}\mid_0^b = -e^{-b}+e^0 = 1-e^{-b}$ \textcolor{blue}{$\xrightarrow[]{t\to\infty}1$}
	\item Bsp2 fehlt
	\item Bsp3 fehlt
	\item Bsp4 fehlt
	\item $\int_0^1{\ln(x)}dx=? \int_a^1{\ln(x)}dx=\text{(\ref{ssub:integralrechnung-beispiele})(4)} (x\cdot\ln(x)-x)\mid_a^1 =1\cdot\ln(1) - 1 - a \cdot \ln(a) + a$\textcolor{blue}{$\to -1$} $\Rightarrow \int_0^1{\ln(x)}dx=\underline{-1}$.
	\item Bsp6 fehlt

\end{enumerate}
% subsubsection beispiele (end)

% subsection uneigentliche_integrale (end)
%!TEX root = ../../MBNWSkript.tex
\subsection{Extrema mit Nebenbedingungen} % (fold)
\label{sub:extrema_mit_nebenbedingungen}

Situation: Gegebene Funktion $f_D^{\subseteq \mathbb{R}^n} \to \mathbb{R}$. Man sucht die Extrema von $f$ nicht auf ganz $D$, sondern auf einer Teilmenge von $D$, die durch eine Gleichung $g(\vec x) = 0$ gegeben ist.

\subsubsection{Beispiele} % (fold)
\label{ssub:extrema_mit_nebenbedingungen-beispiele}
\begin{enumerate}[label=(\arabic*)]
	\item Wähle Koordinatensystem im $\mathbb{R}^3$, $f:\mathbb{R}^3 \to \mathbb{R}$,\\$f(x,y,z) :=$ Temperatur im Punkt $\begin{pmatrix}x\\y\\z\end{pmatrix}$. (siehe Abb.~\ref{fig:extrema_mit_nebenbedingungen-bsp-1})
	\begin{figure}[ht!]
		\begin{center}
			\includegraphics[scale=1]{Analysis/ExtremaMitNebenbedingungen/bsp-1}
		\end{center}
		\caption{Skizze}
		\label{fig:extrema_mit_nebenbedingungen-bsp-1}
	\end{figure}
	\\($\infty$-langes) Rohr $R$ um die $x$-Achse mit Radius $r$.\\
	Wo auf der Außenseite des Rohres herrscht die maximale / minimale Temperatur?\\
	Also gesucht: Maxima / Minima (falls es existiert) der Funktion $f$ auf der Menge
	\[
		\left\{\left(\begin{smallmatrix}x\\y\\z\end{smallmatrix}\right)\in \mathbb{R}^3 \mid y^2+z^2=r^2\right\} = \left\{\left(\begin{smallmatrix}x\\y\\z\end{smallmatrix}\right)\in \mathbb{R}^3 \mid \underbrace{y^2+z^2-r^2}_{\color{blue}g(x,y,z) \text{ "`Nebenbedingung"'}}=0\right\}
	\]
	\item $f:\mathbb{R}^2 \to \mathbb{R}, f(x,y) := x \cdot y$ Gesucht: Extrema von $f$ auf der Einheitskreisscheibe (mit Rand).
	\[K:=\left\{\left(\begin{smallmatrix}x\\y\end{smallmatrix}\right)\in \mathbb{R}^2 \mid x^2 + y^2 \leq 1 \right\}=\bar{B}_1(\vec 0)\]
	Extrema im Inneren von $K$: Benutze die Methoden aus §\ref{sub:extrema_von_funktionen_in_mehreren_variablen}. \\
	(Gradient, statische Punkte, Hesse-Matrix, ...) Bestimme dann die Randextrema, also die Extrema von $f$ auf $dK = \left\{ \left(\begin{smallmatrix}x\\y\end{smallmatrix}\right)\in \mathbb{R}^2\mid x^2+y^2=1\right\}= \left\{ \left(\begin{smallmatrix}x\\y\end{smallmatrix}\right)\in \mathbb{R}^2\mid x^2+y^2-1=0\right\}$
\end{enumerate}

\subsubsection{Extrema mit Nebenbedingungen} % (fold)
\label{ssub:extrema_mit_nebenbedingungen-extrema_mit_nebenbedingungen}
\fbox{
\parbox{\columnwidth}{
	Gegeben: $d \subseteq \mathbb{R}^n$ offen, $f,g:D\to \mathbb{R}$ stetig partiell differenzierbare Funktionen\\
	Gesucht: Die Extrema von $f$ auf $\left\{ \vec x \in D \mid g(\vec x ) = 0 \right\}$. Man sagt: "`Wir suchen die Extrema von $f$ unter der Nebenbedingung $g(\vec x) = 0$."'
}}
% subsubsection extrema_mit_nebenbedingungen (end)
\phantom{}\vspace{3mm}\\
Zu \eqref{ssub:extrema_mit_nebenbedingungen-beispiele}(2) ist der 2. Schritt (Bestimmung der Randextrema) ein Beispiel für "`Extrema unter Nebendedingungen"' im Sinne von \eqref{ssub:extrema_mit_nebenbedingungen-extrema_mit_nebenbedingungen}.
% subsubsection beispiele (end)

\subsection*{Methode zur Berechnung von Extrema unter Nebenbedingungen} % (fold)
\label{sub:extrema_mit_nebenbedingungen-methode_zur_berechnung_von_extrema_unter_nebenbedingungen}
\begin{enumerate}[label=(\arabic*), series=methode_zur_berechnung_von_extrema_unter_nebenbedingungen]
	\item \underline{Einsetzverfahren}:\\
	Betrachte den Fall $n=2$ {\color{blue}(d.h. $f, g$ hängen nur von zwei Variablen $x, y$ ab.)}. Versuche, die Gleichung $g(x,y)=0$ nach einer Variablen aufzulösen, z.B. $y=h(x)$. Ersetze $y$ durch $h(x)$ in $f(x,y)$. $\rightsquigarrow$ Erhalte $\tilde{f}(x)=f(x,h(x))$. Bestimme die Extrema von $\tilde{f}.$
\end{enumerate}
% subsection methode_zur_berechnung_von_extrema_unter_nebenbedingungen_ (end)

\subsubsection{Beispiele} % (fold)∫
\label{ssub:extrema_mit_nebenbedingungen-beispiele-2}
\begin{enumerate}[label=(\arabic*)]
	\item Gesucht: Extrema von $f:\mathbb{R}^2 \to \mathbb{R}$, $f(x,y) := x^2 \cdot y$ unter der Nebenbedingung $g(x,y)=x+y-3=0.$ (*)\\
			\underline{Hierzu}: Löse (*) nach $y$ auf: $y=3-x$. Ersetze $y$ durch $3-x$ in $f(x,y)$:
			\[\tilde{f}(x)=x^2\cdot (3-x)=3x^2-x^3\]
			\[\tilde{f}'(x)=6x-3x^2=3x(2-x) \text{ Also: }\tilde{f}'(x)=0 \Leftrightarrow x=0\text{ oder }x=2\]
			\[\tilde{f}''(x)=6-6x \Rightarrow \tilde{f}''(0)=6 > 0, \tilde{f}''(2) = -6 < 0 \text{ Also:}\]
			\[\tilde{f} \text{ hat }\left.\begin{matrix}\text{* lokales Minimum in 0 mit Funktionswerten }\tilde{f}(0)=0\\\text{* lokales Maximum in 2 mit Funktionswerten }\tilde{f}(2)=4\end{matrix}\right\}\text{keine globale Extrema}\]\[\text{wegen }\lim_{x\to \pm \infty}{\tilde{f}(x)}=\pm\infty\]
			Also: \hspace{0.025\columnwidth}
			\fbox{
			\parbox{0.875\columnwidth}{
				$f$ hat in
				\begin{itemize}
					\item $(0, 3)$ lokales Minimum unter der NB $g(x,y)=0, f(0, 3)=0$.
					\item $(2, 1)$ lokales Maximum unter der NB $g(x,y)=0, f(2,1)=4$.
				\end{itemize}
			}}\phantom{}\vspace{3mm}\\
			Auflösen von $g(x,y)=0$ nach $x$ oder $y$ kann schwierig / aufwendig sein.\\\phantom{}[$\to$ Fallunterscheidung]
			\item TODO
\end{enumerate}
% subsubsection beispiele (end)

\subsubsection{Beispiele} % (fold)
\label{ssub:extrema_mit_nebenbedingungen-beispiele-3}
\begin{enumerate}[label=(\arabic*)]
	\item TODO!
\end{enumerate}
% subsubsection beispiele (end)

% subsection extrema_mit_nebenbedingungen (end)
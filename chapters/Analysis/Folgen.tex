%!TEX root = ../../MBNWSkript.tex

\subsection{Folgen}
%7.1
\subsubsection{Definition}
\fbox{\parbox{\columnwidth}{
Eine reelle Folge ist eine Abbildung $a$, die jedem $n\in\mathds{N}$ eine reelle Zahl $a_n$ zuordnet.\\
Schreibweise: ${(a_n)}_{n\in\mathds{N}}$ oder ${(a_n)}_n$ oder $(a_n)$
}}
\\ \\
%7.2
\subsubsection{Beispiel}
\begin{itemize}
\item[(1)] $a_n:=2n\to(a_n)=(2,4,6,8,\dots)$

\item[(2)] $a_n:=\frac{1}{n}\to(a_n)=(1,\frac{1}{2},\frac{1}{3},\frac{1}{4},\dots)$

\item[(3)] $a_n:=x^n$ ($x\in\mathds{R}$ fest gewählt) $\to(a_n)=(x,x^2,x^3,x^4,\dots)$ \\
Spezialfälle: $x=1:$ $(a_n)=(1,1,1,1,\dots)$ \\
\phantom{Spezialfälle: }$x=-1:$ $(a_n)=(-1,1,-1,1,-1,1,\dots)$

\item[(4)] Nummeriere die Handelstage seit Einführung des DAX mit $n=1,2,3,\dots$ durch.\\
$a_n:=$ \textit{Schlusskurs des DAX am Tag} $n$.
\end{itemize}
Rekursiv definierte Folgen:
\begin{itemize}
\item[(5)] $a_1:=1,a_{n+1}:=3\cdot a_n+1\to(a_n)=(1,4,13,40,\dots)$

\item[(6)] $a_1:=1, a_2:=2, a_{n+1}:=a_n+a_{n-1}\to(a_n)=(1,2,3,5,8,13,\dots)$ \\
\end{itemize}
\underline{Anmerkung:}\\
Folgen müssen nicht mit Index $1$ beginnen, z.B. ${(a_n)}_{n\geq2}=\frac{1}{n^2-1}$.\\
(Folgen können auch mit Index 0 beginnen, ${(a_n)}_{n\in\mathds{N}_0}$)
\\ \\
%7.3
\subsubsection{Definition}
\fbox{\parbox{\columnwidth}{
Sei $(a_n)$ eine Folge.
\begin{itemize}
\item[(a)] $(a_n)$ heißt konvergent mit Grenzwert $a\in\mathds{R}$, falls es zu jedem $\varepsilon>0$ ein $N\in\mathds{N}$ mit $|a_n-a|<\varepsilon$ für alle $n\geq N$. Man sagt dann auch $(a_n)$ konvergiert gegen $a$ und schreibt $\lim\limits_{n \rightarrow \infty}{a_n}=a$ oder $a_n\xlongrightarrow{n\to\infty}a$ oder $a_n\to a$.

\item[(b)] Ist $(a_n)$ nicht konvergent, so nennt man $(a_n)$ divergent

\item[(c)] Gilt $\lim\limits_{n \rightarrow \infty}{a_n}=0$, so nennt man $(a_n)$ eine Nullfolge.
\end{itemize}
}}
%Bild einfügen!!
%\begin{figure}[h]
%\centering
%\includegraphics[]{Analysis/Folgen/Skizze_7.3}
%\label{asdl}
%\end{figure}
%7.4
\subsubsection{Beispiel}
\begin{itemize}
\item[(1)] $a_n:=\frac{1}{n}\Rightarrow(a_n)$ konvergiert gegen 0 (also $(a_n)$ ist eine Nullfolge)\\
Beweis: Sei $\varepsilon>0.\Rightarrow$ Es existiert $n\in\mathds{N}$ mit $N>\frac{1}{\varepsilon}.[\Rightarrow\frac{1}{N}<\varepsilon]$\\
Also für alle $n\geq N$: $|a_n-0|=\left| \frac{1}{n}-0\right|=\frac{1}{n}\leq\frac{1}{N}<\varepsilon$

\item[(2)] $a_n:=(-1)^n$\\
$(a_n)$ divergent, denn als Grenzwerte kommen nur $\pm1$ in Frage (da unendlich viele $a_n$'s gleich $\pm1$ sind), aber unendlich viele $a_n$'s haben zu 1 Abstand $>\frac{1}{3}$ und unendlich viele $a_n$'s haben zu $-1$ Abstand $>\frac{1}{3}$. \\
\end{itemize}
\underline{Anmerkung:}\\
Ist eine Folge $(a_n)$ konvergent, so ist ihr Grenzwert eindeutig bestimmt.
\\ \\
%7.5
\subsubsection{Hilfsmittel zur Berechnung von Grenzwerten}
\fbox{\parbox{\columnwidth}{
Seien $(a_n),(b_n)$ konvergente Folgen mit $a_n\to a,b_n\to b$.
\begin{itemize}
\item[(a)] $(a_n + b_n)$ konvergiert gegen $a+b$ und $(a_n-b_n)$ konvergiert gegen $a-b$.

\item[(b)] $(a_n \cdot b_n)$ konvergiert gegen $a\cdot b$.

\item[(c)] Falls $b\neq0$ und $b_n\neq0$ für alle $n\in\mathds{N}$: $\left( \frac{a_n}{b_n}\right)$ konvergiert gegen $\frac{a}{b}$.

\item[(d)] $\left( |a_n|\right)$ konvergiert gegen $|a|$.

\item[(e)] Falls $a_n\geq0$ für alle $n\in\mathds{N}$: $(\sqrt{a_n})$ konvergiert gegen $\sqrt{a}$.

\item[(f)] Gibt es $N\in\mathds{N}$ mit $a_n\leq b_n$ für alle $n\geq N$, so ist $a\leq b$.

\item[(g)] (Einschränkungskriterium) \\
Ist $a=b$ und $(c_n)$ eine Folge mit $a_n\leq c_n\leq b_n$ für alle $n\in\mathds{N}$, so konvergiert auch $(c_n)$ gegen $a$.
\end{itemize}
}}
\\ \\
%7.6
\subsubsection{Beispiel}
\begin{itemize}
\item[(1)] $q\in\mathds{R},|q|<1,a_n:=q^n.$ $\Rightarrow(a_n)$ ist Nullfolge, d.h. $a_n\to0$.
Bsp.: $q:=0,1\to(a_n)=(0,1;0,001;0,0001;\dots)$

\item[(2)] $a_n:=\frac{2}{n}+\left(\frac{1}{3}\right)^n+7=2\cdot\frac{1}{n}+\left(\frac{1}{3}\right)^n+7\xlongrightarrow{n\to\infty}2\cdot0+0+7=7$ \\
$\Rightarrow(a_n)$ konvergiert gegen 7.

\item[(3)] $a_n:=\frac{3n^2+7n+8}{5n^2-8n+1}$\\
Idee: Kürze durch größte Potenz von $n$, sodass im Zähler und Nenner Ausdrücke entstehen, die konvergieren und nicht beide Grenzwert 0 haben. \\
$a_n=\frac{3+\frac{7}{n}+\frac{8}{n^2}}{5-\frac{8}{n}+\frac{1}{n^2}}\xlongrightarrow{n\to\infty}\frac{3+0+0}{5-0-0}=\frac{3}{5}$, also: $(a_n)$ konvergiert gegen $\frac{3}{5}$.

\item[(4)] $a_n:=\frac{3^{n+1}+2^n}{3^n+1}=\frac{3\cdot3^n+2^n}{3^n+1}$\\
Idee wie bei (3): kürze $3^n$.\\
$a_n=\frac{3+\frac{2^n}{3^n}}{1+\frac{1}{3^n}}=\frac{3+\left(\frac{2}{3}\right)}{1+\left(\frac{1}{3}\right)^n}\xlongrightarrow{n\to\infty}\frac{3+0}{1+0}=3$, also: $(a_n)$ konvergiert gegen 3. \\ \\
\end{itemize}
%als section ohne Nummer!!
\underline{Monotonie ohne Beschränktheit:}
%7.7
\subsubsection{Definition}
\fbox{\parbox{\columnwidth}{
Eine Folge ${(a_n)}_{n\in\mathds{N}}$ heißt:
\begin{itemize}
\item nach oben beschränkt, falls es ein $K\in\mathds{R}$ gibt mit $a_n\leq K$ für alle $n\in\mathds{N}$.

\item nach unten beschränkt, falls es ein $K\in\mathds{R}$ gibt mit $a_n\geq K$ für alle $n\in\mathds{N}$

\item beschränkt, falls es ein $K\in\mathds{R}$ gibt mit $|a_n|\leq K$ für alle $n\in\mathds{N}$

\item monoton wachsend, falls $a_{n+1}\geq a_n$ für alle $n\in\mathds{N}$.

\item streng monoton wachsend, falls $a_{n+1}> a_n$ für alle $n\in\mathds{N}$.

\item monoton fallend, falls $a_{n+1}\leq a_n$ für alle $n\in\mathds{N}$.

\item streng monoton fallend, falls $a_{n+1}< a_n$ für alle $n\in\mathds{N}$.
\end{itemize}
}}
\\ \\
%7.8
\subsubsection{Anmerkung}
\begin{itemize}
\item[(a)] $(a_n)$ ist dann und nur dann beschränkt, wenn $(a_n)$ nach oben und nach unten beschränkt ist.

\item[(b)] Eine Zahl $K\in\mathds{R}$ mit $a_n\leq K$ bzw. $a_n\geq K$ für alle $n\in\mathds{N}$ heißt obere bzw. untere Schranke für $(a_n)$.
\\
\end{itemize}
%
\underline{Bsp.:}
\begin{itemize}
\item[(1)] $a_n:=(-1)^n\Rightarrow (a_n)$ beschränkt, nicht monoton.

\item[(2)] $(a_n):=(-2)^n\Rightarrow(a_n)$ unbeschränkt, nicht monoton.

\item[(3)] $a_n:=1+\frac{1}{n}$ Für alle $n\in\mathds{N}$: $1\leq a_n\leq 1+1=2\Rightarrow(a_n)$ beschränkt.\\
Für alle $n\in\mathds{N}$: $a_{n+1}-a_n=1+\frac{1}{n+1}-\left(1+\frac{1}{n}\right)=\frac{1}{n+1}-\frac{1}{n}=\frac{n-(n+1)}{n(n+1)}=-\frac{1}{n(n+1)}<0$\\
$\Rightarrow(a_n)$ streng monoton fallend.
\\
\end{itemize}
%
\underline{Anmerkung:}
\begin{itemize}
\item[(a)] Manchmal ist es nützlich $\frac{a_{n+1}}{a_n}\geq1$ statt $a_{n+1}-a_n\geq0$ zu zeigen.

\item[(b)] Hat man eine Vermutung für eine obere/untere Schranke K bzw. für das Monotonieverhalten einer Folge, so kann man versuchen, mit vollständiger Induktion $a_n\leq K$ oder $a_n\geq K$ bzw. $a_{n+1}\leq a_n$ oder $a_{n+1}\geq a_n$ nachzuweisen.
\\
\end{itemize}
%
\underline{Bsp.:}\\
$a_1:=1,a_{n+1}:=\sqrt{2\cdot a_n}$. Berechne $a_1,\dots,a_10$ $\to$ Vermutung: $1\leq a_n\leq2$.\\
Vollständige Induktion(*) $\Rightarrow$ Vermutung richtig für alle $n\in\mathds{N}\Rightarrow(a_n)$ beschränkt.\\
Für alle $n\in\mathds{N}$: $\frac{a_{n+1}}{a_n}=\frac{\sqrt{2\cdot a_n}}{a_n}=\frac{\sqrt{2\cdot a_n}}{\sqrt{{a_n}^2}}=\sqrt{\frac{2\cdot a_n}{{a_n}^2}}\geq1\Rightarrow(a_n)$ monoton wachsend.\\
\\
(*): Induktionsannahme: $n=1$: $1\leq a_1\leq2$ $\surd$\\
\phantom{(*):} Induktionsschritt: $n\to n+1$: $1\leq a_{n+1}=\underbrace{\sqrt{2\cdot a_n}}_{\geq1,\leq2 \text{ nach I.A.}}\leq2$
\\ \\
%7.9
\subsubsection{Konvergenzkriterien}
\fbox{\parbox{\columnwidth}{
Sei $(a_n)$ eine Folge.
\begin{itemize}
\item[(a)] $(a_n)$ konvergent $\Rightarrow (a_n)$ beschränkt.

\item[(b)] (Monotonieprinzip) Ist $(a_n)$ monoton (d.h. monoton wachsend oder monoton fallend) und beschränkt, so ist $(a_n)$ konvergent.

\item[(c)] (Cauchy-Kriterium) $(a_n)$ ist dann und nur dann konvergent, wenn es zu jedem $\varepsilon>0$ ein $N\in\mathds{N}$ gibt mit $|a_n-a_m|<\varepsilon$ für alle $n,m\geq N$.
\end{itemize}
}}
\\ \\ \\
\underline{Bsp.:}\\
$a_1:=1, a_{n+1}:=\sqrt{2\cdot a_n}\xlongrightarrow{\text{s.o.}} (a_n)$ monoton und beschränkt $\xlongrightarrow{\text{(7.9)(b)}}(a_n)$ konvegiert gegen $a\in\mathds{R}$\\
$a_{n+1}=\sqrt{2\cdot a_n}\Rightarrow a=\sqrt{2\cdot a}\Rightarrow a^2=2\cdot a\Rightarrow a^2-2\cdot a=0$\\
%durchgestrichener Text!!
\phantom{$a_{n+1}=\sqrt{2\cdot a_n}$} $\Rightarrow a\cdot (a-2)=0\Rightarrow$ $a=0$ oder $a=2\Rightarrow (a_n)$ konvergiert gegen 2.
\\ \\
\underline{Bestimmte Divergenz:}\\
\underline{Bsp.:} $(a_n)=\underbrace{(1,2,3,4\dots)}_{\text{strebt gegen }\infty}$, $(b_n)=(1,\frac{1}{1},2,\frac{1}{2},3,\frac{1}{3},4,\frac{1}{4},\dots)$
\\ \\
%7.10
\subsubsection{Definition}
\fbox{\parbox{\columnwidth}{
Eine Folge $(a_n)$ heißt bestimmt divergent
\begin{itemize}
\item gegen $\infty$, falls für jedes $K\in\mathds{R}$ ein $N\in\mathds{R}$ existiert, mit $a_n>K$ für alle $n\geq\mathds{N}$.

\item gegen $-\infty$, falls für jedes $K\in\mathds{R}$ ein $N\in\mathds{R}$ existiert, mit $a_n<K$ für alle $n\geq\mathds{N}$.
\end{itemize}
Schreibweise: $\lim\limits_{n \rightarrow \infty}{a_n}=\pm\infty$ oder $a_n\xlongrightarrow{n\to\infty}\pm\infty$ oder $a_n\to\pm\infty$.
}}
\\ \\
%7.11
\subsubsection{Anmerkung}
\begin{itemize}
\item[(a)] Aus $a_n\to\pm\infty$ und $a_n\neq0$ für alle $n\in\mathds{N}$ folgt: $\frac{1}{a_n}\to0$.

\item[(b)] Aus $a_n\to0$ und $a_n\neq0$ für alle $n\in\mathds{N}$ folgt: $\frac{1}{a_n}\to\left\{\begin{array}{cl} +\infty, & \text{falls }a_n>0\text{ für alle }n\in\mathds{N}\\ -\infty, & \text{falls }a_n<0\text{ für alle }n\in\mathds{N} \end{array}\right. $

\item[(c)] Sei $(a_n)$ eine Folge mit $a_n\to\infty$ und $b_n$ eine Folge mit $b_n\to b\in\mathds{R}$ oder $b\to\infty$.\\
Dann gilt:
\begin{itemize}
\item[(i)] $a_n+b_n\to\infty$

\item[(ii)] $a_n\cdot b_n\to\left\{\begin{array}{cl} 
+\infty, & \text{falls }b_n\to\infty\text{ oder }b_n\to b\text{ mit }b>0\\ 
-\infty, & \text{falls }b_n\to b\text{ mit }b_n<0 
\end{array}\right.$
\\ \\
\end{itemize}
\end{itemize}
\underline{Bsp.:}
\begin{itemize}
\item[(1)] $a_n:=n^2-\frac{1}{n}\Rightarrow a_n\to\infty \hspace{10mm} a_n:=-n\Rightarrow a_n\to\infty$

\item[(2)] $a_n:=q^n$ $(q\in\mathds{R},q>1)\Rightarrow a_n\to\infty$

\item[(3)] $a_n:=(-2)^n\Rightarrow (a_n)$ nicht bestimmt divergent.
\\ \\
\end{itemize}
\underline{Häufungspunkte:}\\
Die Werte von $(1,\frac{1}{1},2,\frac{1}{2},3,\frac{1}{3},\dots)$ häufen sich bei 0, aber "{}ein Teil der Werte bleibt nicht dort.{}"
\\ 
%7.12
\subsubsection{Definition}
\fbox{\parbox{\columnwidth}{
Sei ${(a_n)}_{n\in\mathds{N}}$ eine Folge.
\begin{itemize}
\item[(a)] Ist ${(n_k)}_{k\in\mathds{N}}$ eine streng monoton wachsende Folge natürlicher Zahlen, so heißt ${(a_{n_k})}_{k\in\mathds{N}}$ eine Teilfolge von $(a_n)$

\item[(b)] $a\in\mathds{R}$ heißt Häufungspunkt von $(a_n)$, wenn eine Teilfolge ${(a_{n_k})}_{k\in\mathds{N}}$ von $(a_n)$ existiert mit $a_{n_k}\xlongrightarrow{k\to\infty}a$
\end{itemize}
}}
\\ \\ \\
\underline{Bsp.:}
\begin{itemize}
\item[(1)] $a_n:=(-1)^n\Rightarrow (a_n)$ hat die Häufungspunkte 1 und -1, denn ${(a_{2k})}_{k\in\mathds{N}}$ konvergiert gegen 1 und ${(a_{2k+1})}_{k\in\mathds{N}}$ konvergiert gegen -1.

\item[(2)] $(a_n)=(1,\frac{1}{1},2,\frac{1}{2},3,\frac{1}{3},\dots)$ hat den Häufungspunkt 0, denn ${(a_{2k})}_{k\in\mathds{N}}=(\frac{1}{1},\frac{1}{2},\frac{1}{3},\frac{1}{4},\dots)$ ist eine Nullfolge.
\\ \\
\end{itemize}
%7.13
\subsubsection{Satz (Bolzano-Weierstraß)}
\fbox{\parbox{\columnwidth}{
Jede beschränkte Folge $(a_n)$ hat eine konvergente Teilfolge, also mindestens einen Häufungspunkt.
}}
\\
%7.14 fix this!! Formatierung schlecht!
\subsubsection{Definition}
\fbox{\parbox{\columnwidth}{
Für eine Folge $a_n$ setze:
\begin{itemize}
\item $\lim\limits_{n \rightarrow \infty}{\text{\textit{sup} }a_n}:=\left\{\begin{array}{cl} 
\phantom{-}\infty, & \text{falls }a_n\text{ nach oben unbeschränkt }\\ 
-\infty, & \text{falls }a_n\to-\infty\\
\text{größter}&\text{Häufungspunkt von }a_n\text{, sonst (**)}
\end{array}\right.$

\item $\lim\limits_{n \rightarrow \infty}{\text{\textit{inf} }a_n}:=\left\{\begin{array}{cl} 
-\infty, & \text{falls }a_n\text{ nach unten unbeschränkt }\\ 
\phantom{-}\infty, & \text{falls }a_n\to\infty\\
\text{kleinster}&\text{Häufungspunkt von }a_n\text{, sonst (***)}
\end{array}\right.$
\end{itemize}
(Man kann zeigen, dass in den Fällen (**), (***) ein größter bzw. kleinster Häufungspunkt von $(a_n)$ existiert.)
}}
\\ \\ \\
\underline{Bsp.:}
\begin{itemize}
\item[(1)] $a_n:=(-1)^n\Rightarrow \lim\text{\textit{sup} }a_n=1$, $\lim\text{ \textit{inf} }a_n=-1$

\item[(2)] $(a_n)=(1,\frac{1}{1},2,\frac{1}{2},3,\frac{1}{3},\dots)\Rightarrow \lim\text{\textit{sup} }a_n=\infty$, $ \lim\text{\textit{inf} }a_n=0$
\\
\end{itemize}
\underline{Anmerkung:}\\
Wozu sind Folgen gut? Oft lassen sich "{}nichtlineare{}" Gleichungen nicht explizit lösen. Idee: Konstruiere Folge $(x_n)$, die gegen die Lösung $x$ der Gleichung konvergiert und berechne damit $x$ näherungsweise.


\newpage
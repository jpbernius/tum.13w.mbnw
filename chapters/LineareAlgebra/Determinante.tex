%!TEX root = ../../MBNWSkript.tex

\subsection {Die Determinante}
Die Determinante ist eine Abbildung, die jeder quadratischen Matrix $A\in\mathds{R}^{n\times n}$ eine Zahl $det(A)\in\mathds{R}$ zuordnet, sodass $A$ dann und nur dann invertierbar ist, wenn $det(A)\neq0$.
\\ \\
Für $A\in\mathds{R}^{n\times n}$ und $1\leq i,j\leq n$ sei $A_{ij}\in\mathds{R}^{(n-1)\times(n-1)}$ die Matrix, die aus $A$ durch Streichen der $i$-ten Zeile und $j$-ten Spalte entsteht.
\\ \\
%3.1
\subsubsection{Definition}
\fbox{\parbox{\columnwidth}{
	Sei $A\in\mathds{R}^{n\times n}$. Wir definieren die Zahl $det(A)\in\mathds{R}$ rekursiv so:
	\begin{itemize}
		\item[] Falls $n=1$: $det(A):=a_{11}$

		\item[] Falls $n>1$: $det(A):=a_{11}\cdot det(A_{11})-a_{21}\cdot det(A_{21})\pm...+(-1)^{n+1}a_{n1}\cdot det(A_{n1})$
	\end{itemize}
	Schreibe oft $|A|$ statt $det(A)$.
}}
\\ \\ \\
\underline{Bsp.:}
\begin{itemize}
	\item[(1)] 
		$det
		\left(
		\begin{array}{cc}
			2^{(+)}   &6   \\
			1^{(-)}   &5
		\end{array}
		\right)=2\cdot 5-1\cdot 6=4$

	\item[(2)] 
		$det
		\left(
		\begin{array}{ccc}
  			1^{(+)} &2 &3 \\
  			6^{(-)} &5 &4 \\
 			7^{(+)} &8 &9
		\end{array}
		\right)=1\cdot
		\left|
		\begin{array}{cc}
  			5   &4   \\
  			8   &9
		\end{array}
		\right|-6\cdot
		\left|
		\begin{array}{cc}
 			 2   &3   \\
 			 8   &9
		\end{array}
		\right|
		+7\cdot
		\left|
		\begin{array}{cc}
  			2   &3   \\
  			5   &4
		\end{array}
		\right|=0$
\\ \\
\end{itemize}
%3.2
\subsubsection{Wichtige Spezialfälle}
\fbox{\parbox{\columnwidth}{
\begin{itemize}
	\item[(a)] $det
		\left(
		\begin{array}{cccc}
  			\lambda_{1} & & &*\\
 			&\lambda_{2} & & \\
   			& &\ddots & \\
  			\smash{\makebox{\hspace{3mm}\Huge 0\hspace{-3mm}} }& & &\lambda_n
		\end{array}
		\right)=\lambda_1\cdot\lambda_2\cdot...\cdot\lambda_n$, $det\left(
		\begin{array}{cccc}
  			\lambda_{1} & & & {\makebox{\Huge 0} }\\
   			&\lambda_{2} & & \\
   			& &\ddots & \\
   			*& & &\lambda_n
		\end{array}
		\right)=\lambda_1\cdot\lambda_2\cdot...\cdot\lambda_n$ \\
		\phantom{}\hspace{37mm}\rotatebox[origin=c]{90}{$\Rsh$}"{}Dreiecksmatrizen{}" $\in\mathds{R}^{n\times n}$\rotatebox[origin=c]{270}{$\Lsh$}

	\item[(b)] Für $A=\left(
		\begin{array}{cc}
 			B   &C   \\
  			0   &D
		\end{array}
		\right)$ oder $A=\left(
		\begin{array}{cc}
 			B   &0   \\
  			C   &D
		\end{array}
		\right)\in\mathds{R}^{n\times n}$ \\
		mit quadratischen Matrizen $B,D$ und Matrizen 0 (=Nullmatrix), $C$ der passenden Größe ist $det(A)=det(B)\cdot det(C)$

	\item[(c)] Für alle $A=\left(
		\begin{array}{cc}
  			a   &b   \\
 			c   &d
		\end{array}
		\right)\in\mathds{R}^{2\times2}$ ist $det(A)=ad-bc$

	\item[(d)] (Sarrus-Regel) Für alle $A=\left(
		\begin{array}{ccc}
 			a &b &c  \\
  			d &e &f \\
  			g &h &i
		\end{array}
		\right)\in\mathds{R}^{3\times3}$ \\
		ist $det(A)=aei+bfg+cdh-ceg-bdi-afh$.
\end{itemize}
}}
\\ \\ \\
\underline{Bsp.:} \\
$det\left(
\begin{array}{cccc}
  	\phantom{-}1 &2 &0 &0  \\
  	\phantom{-}1 &1 &0 &0 \\
 	 -7 &8 &2 &3 \\
 	 \phantom{-}1 &2 &0 &3
\end{array}
\right)=\left|
\begin{array}{cc}
 	 1   &2   \\
  	1   &1
\end{array}
\right|\cdot\left|
\begin{array}{cc}
 	 2   &3   \\
 	 0   &3
\end{array}
\right|=(1-2)\cdot2\cdot3=-6$
\\ \\
%3.3
\subsubsection{Rechenregeln für die Determinante}
\fbox{\parbox{\columnwidth}{
	Sei $A\in\mathds{R}^{n\times n}$. Dann gilt:
	\begin{itemize}
		\item[(a)] für $1\leq i\leq n$: $det(A)=\sum_{j=1}^n(-1)^{i+j}a_{ij}\cdot det(A_{ij})$ \hspace{5mm}"{}Entwickeln nach $i$-ter Zeile{}"

		\item[(b)] für $1\leq j\leq n$: $det(A)=\sum_{i=1}^n(-1)^{i+j}a_{ij}\cdot det(A_{ij})$ \hspace{5mm}"{}Entwickeln nach $j$-ter Spalte{}"

		\item[(c)]
			\begin{itemize}
				\item Vertauscht man zwei Zeilen oder Spalten von $A$, so ist:\\
				$det(\text{\textit{neue Matrix}})=-det(A)$.

				\item Addiert man das $\lambda$-fache einer Zeile oder Spalte zu einer anderen, so ist:\\
				$det(\text{\textit{neue Matrix}})=det(A)$.
			\end{itemize}
		\end{itemize}
}}
\\ \\ \\
\underline{Bsp.:}
\begin{itemize}
	\item[(1)] $det\left(
	\begin{array}{ccc}
  		3 &\phantom{-}3 &\phantom{-}1  \\
  		1 &\phantom{-}0 &-1 \\
 		4 &-1 &\phantom{-}0
	\end{array}
	\right)\overset{\text{Entwickeln nach}}{\underset{\text{3. Spalte}}{=}}1\cdot\left|
	\begin{array}{cc}
		1   &\phantom{-}0   \\
  		4   &-1
	\end{array}
	\right|-(-1)\cdot\left|
	\begin{array}{cc}
  		3   &\phantom{-}3   \\
  		4   &-1
	\end{array}
	\right|+0\cdot\left|
	\begin{array}{cc}
  		3   &3   \\
  		1   &0
	\end{array}
	\right|\\
	\phantom{}\hspace{10mm}=-1+(-3-12)=-16$

	\item[(2)]
	$\begin{pmatrix}
		\phantom{-}2 &\phantom{-}3 &4 &\phantom{-}0 \\
		-6 &-3 &2 &-5 \\
		\phantom{-}0 &\phantom{-}0 &1 &\phantom{-}0 \\
		-2 &\phantom{-}5 &9 &-2
	\end{pmatrix}\overset{\text{Entwickeln nach}}{\underset{\text{3. Zeile}}{=}}1\cdot
	\begin{gmatrix}[v]
  		\phantom{-}2   &\phantom{-}3 &\phantom{-}0   \\
  		-6   &-3 &-5 \\
  		-2 &\phantom{-}5 &-2
  		\rowops
  			\add[\cdot3]{0}{1}
 			\add[]{0}{2}
	\end{gmatrix}=
	\begin{vmatrix}
		2 &3 &\phantom{-}0 \\
		0 &6 &-5 \\
		0 &8 &-2
	\end{vmatrix}\\
	\phantom{}\hspace{10mm}=
	\begin{vmatrix}
		6 &-5 \\
		8 &-2
	\end{vmatrix}=2\cdot(-12+40)=56$
\\ \\
\end{itemize}
%3.4
\subsubsection{Mögliche Strategie zur Berechnung von $det(A)$ für $A\in\mathds{R}^{n\times n}$}
\fbox{\parbox{\columnwidth}{
	\begin{itemize}
		\item in einer Zeile/Spalte viele Nullen erzeugen (entwicke dann nach dieser Zeile/Spalte)
		
		\item[]\hspace{10mm}oder
		
		\item die Berechnung auf eine (Blocks-)Dreiecksmatrix zurückführen.
	\end{itemize}
}}
\\ \\ \\
%3.5
\subsubsection{Eigenschaften der Determinante}
\fbox{\parbox{\columnwidth}{
	Seien $A,B\in\mathds{R}^{n\times n}$ quadratische Matrizen. Dann gilt:
	\begin{itemize}
		\item[(a)] $det(A\cdot B)=det(A)\cdot det(B)$
		
		\item[(b)] $det(A^T)=det(A)$
		
		\item[(c)] $A$ ist dann und nur dann invertierbar, wenn $det(A)\neq0$. In diesem Fall ist $det(A^{-1})=\frac{1}{det(A)}$.
	\end{itemize}
}}
\\ \\ \\
Wozu ist die Determinante gut? Können wir erst später verstehen. Die Determinante spielt eine wichtige Rolle bei "{}Eigenwertproblemen{}" (z.B. bei schwingenden Systemen in Physik/Technik).


























































\newpage